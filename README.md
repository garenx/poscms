# poscms

#### 介绍
POSCMS 基于PHP+MYSQL+CI框架开发的开源Web内容管理系统，完美兼容PHP7，并在PHP7基础上做了性能优化，系统更加稳定，操作人性化、功能强大、扩展性强，二次开发及后期维护方便，可以帮您快速构建起一个强大专业的WEB网站系统、微信网站、APP服务端等应用。


#### 运行环境：

1. php5.3 - php7
2. mysql5以上

#### 安装方法：

1. 访问http://网站/install.php
2. 账号密码都是admin
3. 安装完成请更新缓存


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
